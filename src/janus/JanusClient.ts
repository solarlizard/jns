import * as rx from "rxjs/Rx"
import * as WebSocket from "ws"
import { JanusRequest } from "./JanusRequest";
import { JanusSession } from "./session/JanusSession";
import { JanusResponse } from "./JanusResponse";
import { JanusRequestBuilder } from "./JanusRequestBuilder";
import {v4 as uuid} from "uuid";
import { Logger } from "../log/Logger";
import { OutboundWebSocket } from "../ws/OutboundWebSocket";
import { Context } from "../util/Context";
import { inject } from "../util/inject";

export class JanusClient {

    public readonly logger = new Logger (JanusClient.name);

    public readonly socket = new OutboundWebSocket (new WebSocket (this.url, 'janus-protocol'));

    public readonly getResponseObservable = () => this.socket.getMessageObservable ()
        .map (response => JSON.parse (response) as JanusResponse)

    @inject private readonly requestBuilder : JanusRequestBuilder;
    
    public constructor (private readonly url : string, context? : Context<any>) {

        if (context) {
            this.socket.bind (context)
        }
        
        this.getResponseObservable ()
            .do (response => this.logger.info ("janus", {response}))
            .takeUntil (this.socket.getCloseObservable ())
            .subscribe (this.logger.rx.subscribe ("Error logging responses"))
    }

    public readonly bind = (context : Context<any>) => this.socket.bind (context)

    public readonly connect = () => this.socket.connect ()
        .flatMap (this.createSession)

    private readonly createSession =() => this.request (this.requestBuilder.createCreateSessionRequest ())
        .map (response => new JanusSession (this, response.data.id))

    public readonly request = (request: JanusRequest) => {

        request.transaction = uuid ().toString ();
        
        this.logger.info ("janus", {request, transaction : request.transaction})

        return rx.Observable.merge (
            this.listenTransactionIdResponse (request.transaction, this.isWaitForAckResponse (request)),
            this.socket.sendRx (JSON.stringify (request)).ignoreElements (),
        )
        .take (1)
        .catch (error => {
            this.logger.error ('Error janus request', {request, error});
            return rx.Observable.throwError (error);
        }) as rx.Observable<JanusResponse>
    }


    public readonly close = () => {
        this.socket.close ();
    }

    private readonly listenTransactionIdResponse = (transaction : string, ack : boolean) => this.getResponseObservable ()
        .filter(response => {
            if (ack) {
                return response.janus === 'ack'
            }
            else {
                return response.janus !== 'ack'
            }
        })
        .filter(response => response.transaction === transaction)

    private readonly isWaitForAckResponse = (request : JanusRequest) => request.janus === 'keepalive' || request.janus === 'trickle'

}


