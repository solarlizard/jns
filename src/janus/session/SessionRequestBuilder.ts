import { JanusRequest } from "../JanusRequest"

export class SessionRequestBuilder {

    public readonly createKeepaliveRequest = () : JanusRequest => ({
        janus : 'keepalive'
    })

    public readonly createAttachPluginRequest = () : JanusRequest => ({
        janus: "attach",
        plugin: "janus.plugin.streaming"
    });
}