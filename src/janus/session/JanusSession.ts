import * as rx from "rxjs/Rx"

import { JanusRequest } from "../JanusRequest";
import { JanusClient } from "../JanusClient";
import { JanusStreamingPlugin } from "./streaming/JanusStreamingPlugin";
import { SessionRequestBuilder } from "./SessionRequestBuilder";
import { EMPTY } from "rxjs";
import { inject } from "../../util/inject";

export class JanusSession {

    @inject private readonly requestBuilder : SessionRequestBuilder;

    constructor(public readonly janus: JanusClient, private readonly sessionId: string) {
        this.startKeepalive();
    }

    public readonly attachStreamingPlugin = () => this.request(this.requestBuilder.createAttachPluginRequest ())
        .map(response => new JanusStreamingPlugin(this, response.data.id));

    public readonly request = (request: JanusRequest) => this.janus.request (this.updateRequest (request))

    private readonly updateRequest = (request : JanusRequest) => {
        request.session_id = this.sessionId;
        return request
    }
    
    private readonly startKeepalive = () => rx.Observable.interval(5000)
        .startWith (0)
        .concatMap(() => this.request(this.requestBuilder.createKeepaliveRequest())
            .take (1)
            .timeout(5000)
        )
        .catch(error => {
            this.janus.logger.error('Error pinging janus', { error });
            this.janus.close();
            return EMPTY;
        })
        .takeUntil(this.janus.socket.getCloseObservable())
        .subscribe(this.janus.logger.rx.subscribe('Error pinging janus'));

}
