import { JanusSession } from "./JanusSession";
import { JanusRequest } from "../JanusRequest";
import { JanusResponse } from "../JanusResponse";
import { JanusRequestBuilder } from "../JanusRequestBuilder";
import { inject } from "../../util/inject";

export class JanusPluginBase {
    
    @inject private readonly janusRequestBuilder : JanusRequestBuilder;

    public readonly getIceObservable = () => this.session.janus.getResponseObservable ()
        .filter (response => response.janus === 'trickle' && response.sender === this.handleId && response.candidate.completed == null)
        .map<JanusResponse,RTCIceCandidateInit>(response => response.candidate as RTCIceCandidateInit)

    public readonly trickle = (ice: RTCIceCandidateInit) => this.request(this.janusRequestBuilder.createTrickleRequest (ice))

    constructor(public readonly session: JanusSession, private readonly handleId: string) {
    }

    public readonly request = (request: JanusRequest) => this.session.request(this.updateRequest (request));

    private readonly updateRequest = (request : JanusRequest) => {
        request.handle_id = this.handleId;

        return request;
    }

}
