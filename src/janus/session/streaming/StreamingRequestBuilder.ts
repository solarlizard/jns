import { JanusRequest } from "../../JanusRequest"
import { CreateStreamRequest } from "./CreateStreamRequest"
import { SdpParser } from "./SdpParser"

export class StreamingRequestBuilder {

    public readonly createListeStreamsRequeat = () : JanusRequest => ({
        janus: "message",
        body: {
            request : "list"
        }
    })
    
    public readonly createAnswerStreamRequest = (answer: string) : JanusRequest => ({
        janus: "message",
        body: {
            request: 'start',
        },
        jsep: {
            sdp : answer,
            type : 'answer'
        }
    })

    public readonly createWatchStreamRequeat = (id : number) : JanusRequest => ({
        janus: "message",
        body: {
            request: 'watch',
            id
        }
    })
    public readonly createCreateStreamRequest = (request : CreateStreamRequest) : JanusRequest => {
        const sdpParser = new SdpParser (request.sdp);

        const result : JanusRequest = {
            janus: "message",
            body: {
                request : 'create',
                id : request.id,
                type : 'rtp',
                video : true,
                audio : false,
                videoport : request.videoport,
                videopt : sdpParser.getPayloadType (),
                videortpmap :sdpParser.getRtpMap (),
            }
        }

        const fmtp = sdpParser.getFmtp ();

        if (fmtp != null) {
            result.body.videofmtp = fmtp;
        }
        
        return result;
    }

    public readonly createDestroyStreamRequest = (id : number) : JanusRequest => ({
        janus: "message",
        body: {
            request : "destroy",
            id
        }
    })
}