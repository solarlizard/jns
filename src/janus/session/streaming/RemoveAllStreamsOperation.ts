import * as rx from 'rxjs/Rx'
import { JanusConnector } from '../../JanusConnector';
import { JanusStreamingPlugin } from './JanusStreamingPlugin';
import { Logger } from '../../../log/Logger';
import { inject } from '../../../util/inject';

export class RemoveAllStreamsOperation {
    
    private readonly logger = new Logger (RemoveAllStreamsOperation.name);

    @inject private readonly janusConnector : JanusConnector;

    public readonly remove = (url : string) => rx.Observable.of ({})
        .flatMap (() => this.doRemove (url))
        .retryWhen (this.logger.rx.retry ("Error removing streams"));

    private readonly doRemove = (url : string) => this.janusConnector.connectStreaming (url)
        .flatMap (this.destroy)
        .take (1)

    private readonly destroy = (plugin : JanusStreamingPlugin) => plugin.listStreamIds ()
        .concatMap(id => plugin.destroyStream (id))
        .toArray ()
}