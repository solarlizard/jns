export class SdpParser {

    private readonly splited : string []

    constructor (sdp : string) {
        this.splited = sdp.split ("\r\n");
    }

    public readonly getPayloadType = () => {
        const value = this.splited
            .filter (line => line.startsWith ('m=video')) [0]
            .split (" ")
            .reverse() [0]

        return parseInt(value);
    }

    public readonly getRtpMap = () => this.splited
        .filter (line => line.startsWith ('a=rtpmap:')) [0]
        .split (" ")[1]

    public readonly getFmtp = () => {
        const filtered = this.listFmtp ();

        if (filtered.length > 0) {
            return filtered[0].split (" ").slice (1).join ("")
        }
        else {
            return null;
        }
    }

    private readonly listFmtp = () => this.splited 
        .filter (line => line.startsWith ('a=fmtp:'))
}