export interface CreateStreamRequest {
    id: number;
    videoport: number;
    sdp : string;
}
