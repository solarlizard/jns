import { JanusSession } from "../JanusSession";
import { CreateStreamRequest } from "./CreateStreamRequest";
import { JanusPluginBase } from "../JanusPluginBase";
import { StreamingRequestBuilder } from "./StreamingRequestBuilder";
import { inject } from "../../../util/inject";

export class JanusStreamingPlugin extends JanusPluginBase {

    @inject private readonly requestBuilder : StreamingRequestBuilder;

    constructor(session: JanusSession, handleId: string) {
        super (session, handleId)
    }

    public readonly createStream = (model : CreateStreamRequest) => this.request (this.requestBuilder.createCreateStreamRequest (model))

    public readonly destroyStream = (id : number) => this.request (this.requestBuilder.createDestroyStreamRequest (id))
        .map (() => void 0)

    public readonly getOffer = (streamId : number) => this.request (this.requestBuilder.createWatchStreamRequeat (streamId))
        .map (response => response.jsep.sdp)
        
    public readonly setAnswer = (answer : string) => this.request (this.requestBuilder.createAnswerStreamRequest (answer))
        .map (() => void 0)

    public readonly listStreamIds = () => this.request (this.requestBuilder.createListeStreamsRequeat ())
        .flatMap (response => response.plugindata.data.list.map (item => item.id))
}
