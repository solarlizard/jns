import { JanusRequest } from "./JanusRequest";

export class JanusRequestBuilder {

    public readonly createCreateSessionRequest = () : JanusRequest => ({
        janus: "create"
    });

    public readonly createTrickleRequest = (ice : RTCIceCandidateInit) : JanusRequest => ({
        janus: "trickle",
        candidate: ice
    })

}