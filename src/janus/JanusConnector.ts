import * as rx from "rxjs/Rx";
import { JanusClient } from "./JanusClient";
import { JanusStreamingPlugin } from "./session/streaming/JanusStreamingPlugin";
import { Logger } from "../log/Logger";
import { Context } from "../util/Context";
import { inject } from "../util/inject";

export class JanusConnector {

    private readonly logger = new Logger (JanusConnector.name);

    @inject context : Context<any>

    public readonly connectStreaming = (url : string) => rx.Observable.of ({})
        .flatMap (() => {
            const janus = new JanusClient (url, this.context);

            return rx.Observable.merge (
                this.closeOnFinally (janus),
                this.handleSocketError (janus),
                this.handleSocketClosed (janus),
                this.attachStreamingPlugin (janus)
            ) as rx.Observable<JanusStreamingPlugin>
        })
        .retryWhen (this.logger.rx.retry ("Error connecting to janus"))    

    private readonly closeOnFinally = (janus : JanusClient) => new rx.Subject().finally (() => janus.close ());
    
    private readonly handleSocketError = (janus : JanusClient) => janus.socket.getErrorObservable ().catch (this.logger.rx.catch ("Error in janus connection"));
    
    private readonly handleSocketClosed = (janus : JanusClient) => janus.socket.getCloseObservable ().flatMap (() => rx.Observable.throwError ("Janus closed connection"));

    private readonly attachStreamingPlugin = (janus : JanusClient) => janus.connect ().flatMap (session => session.attachStreamingPlugin ())
}