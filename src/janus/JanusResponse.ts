export interface JanusResponse {
    janus?: 'ack' | 'trickle',
    sender?: string,
    candidate?: {
        completed?: boolean,
    },
    transaction?: string,
    data: {
        id: string,
    },
    plugindata?: {
        data?: {
            list?: {
                id: number,
            }[],
        },
    },
    jsep?: RTCSessionDescriptionInit
}
