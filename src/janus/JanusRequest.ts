export interface JanusRequest {
    janus?: 'create' | 'attach' | 'message' | 'trickle' | 'keepalive';
    plugin?: "janus.plugin.streaming";
    transaction?: string;
    session_id?: string;
    handle_id?: string;
    candidate?: RTCIceCandidateInit;
    body?: {
        request?: 'destroy' | 'list' | 'create' | 'watch' | 'start';
        id?: number;
        type?: 'rtp';
        video?: boolean;
        audio?: boolean;
        videoport?: number;
        videortpmap?: string;
        videopt?: number;
        videofmtp?: string;
    };
    jsep?: RTCSessionDescriptionInit;
}
