Часть большой библиотеки для работы с media-сервером janus.

Для компиляции нужна вся либа, ее могу показать со своего компа

Модуль используется для видео-стриминга камеры через WebRTC: 
камера --RTSP--> ffmpeg c++ аддон к ноде --RTP--> Janus --WebRTC--> web/mobile

Структура:
JanusClient.connect () => JanusSession
JanusSession.attachStreamingPlugin () => JanusStreamingPlugin

В тестах:
Unit-тесты для отдельных классов
Интеграционный WebRTС тест стриминга через headless-chrome. Тест успешен, если статистика из PeerConnection сообщает, что пошел байтовывй поток видео.
