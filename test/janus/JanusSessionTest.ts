import * as rx from "rxjs/Rx"
import {suite, skipOnError, timeout} from "mocha-typescript";
import {test} from "mocha-typescript";
import { JanusClient, Logger } from "../../src/";
import { JanusSession } from "../../src/";

@suite export class JanusSessionTest {

    @test async startKeepaliveError () {
        const closeObservable = new rx.ReplaySubject<void> (1);

        // mock
        const janusClientMock : any = {};
        
        janusClientMock.request = () => {
            throw new Error ('test')
        }
        janusClientMock.socket = {
            getCloseObservable : () => new rx.Subject ()
        }

        janusClientMock.logger = new Logger (JanusSessionTest.name)

        janusClientMock.close = () => {
            closeObservable.next ()
        }

        // prepare
        const inst = new JanusSession (janusClientMock as JanusClient, "sessionId");

        // assert
        await closeObservable.take (1).toPromise ()
    }
}
