import * as rx from "rxjs/Rx"

import { JanusClient, Logger } from "../../src";

export class JanusTestBase {

    protected readonly logger = new Logger (JanusTestBase.name);

    public readonly connect = () => rx.Observable.of ({})
        .flatMap (() => {
            const janus = new JanusClient ('ws://127.0.0.1:8080')

            return janus.connect ()
        })
        .retryWhen (this.logger.rx.retry ("Error connecting to janus"))
        .take (1)
        .toPromise ()
}