import * as rx from "rxjs/Rx"
import {suite, skipOnError, timeout, only} from "mocha-typescript";
import {assert} from "chai";
import {test} from "mocha-typescript";
import { JanusConnector, inject } from "../../src";
import { RemoveAllStreamsOperation } from "../../src/";
import { JanusTestBase } from "./JanusTestBase";

@suite export class RemoveAllStreamsOperationTest extends JanusTestBase {

    @inject private readonly connector : JanusConnector;
    @inject private readonly instance : RemoveAllStreamsOperation;

    @test async success () {
        // prepare
        const plugin = await (await this.connect ()).attachStreamingPlugin ().toPromise ()
        
        await plugin.createStream ({
            id : 10,
            videoport : 10,
            sdp : "v=0"
+ "\r\no=- 0 0 IN IP4 127.0.0.1"
+ "\r\ns=Media Presentation"
+ "\r\nc=IN IP4 127.0.0.1"
+ "\r\nt=0 0"
+ "\r\na=tool:libavformat 58.29.100"
+ "\r\nm=video 0 RTP/AVP 96"
+ "\r\na=rtpmap:96 VP8/90000"
+ "\r\na=control:streamid=0"
+ "\r\n"
        }).toPromise ();
        
        await plugin.createStream ({
            id : 11,
            videoport : 11,
            sdp : "v=0"
+ "\r\no=- 0 0 IN IP4 127.0.0.1"
+ "\r\ns=Media Presentation"
+ "\r\nc=IN IP4 127.0.0.1"
+ "\r\nt=0 0"
+ "\r\na=tool:libavformat 58.29.100"
+ "\r\nm=video 0 RTP/AVP 96"
+ "\r\na=rtpmap:96 VP8/90000"
+ "\r\na=control:streamid=0"
+ "\r\na=fmtp: test"
+ "\r\n"
        }).toPromise ();

        // 
        await this.connector.connectStreaming ('ws://127.0.0.1:8080').flatMap (plugin => plugin.listStreamIds ().toArray ())
            .filter (list => list.length == 2)
            .take (1)
            .toPromise ()

        // test
        await this.instance.remove ('ws://127.0.0.1:8080').toPromise ();

        // assert
        await this.connector.connectStreaming ('ws://127.0.0.1:8080').flatMap (plugin => plugin.listStreamIds ().toArray ())
            .filter (list => list.length == 0)
            .take (1)
            .toPromise ()
    }    
}
