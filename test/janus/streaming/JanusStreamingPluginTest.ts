import { JanusTestBase } from "../JanusTestBase";
import { JanusConnector, inject } from "../../../src";
import { only, test, suite } from "mocha-typescript";

@suite export class JanusStreamingPluginTest extends JanusTestBase {

    @inject private readonly connector : JanusConnector;

    @test async success () {
        // prepare
        //const plugin1 = await (await this.connect ()).attachStreamingPlugin ().toPromise ()
        const plugin = await this.connect ().then (session => session.attachStreamingPlugin ().toPromise ())

        await plugin.destroyStream (100).toPromise ();
    }    
}
