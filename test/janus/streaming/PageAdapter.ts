import * as puppeteer from "puppeteer"
import { createTurnPassword } from "../../../src";

export class PageAdapter {
    private static readonly URL = `file://${__dirname}/index.html`;

    private browser : puppeteer.Browser;
    private page : puppeteer.Page;
    
    public readonly stop = async() => {
        await this.page.close ();
        await this.browser.close ();
    }

    public readonly start = async () => {
        this.browser = await puppeteer.launch({
            headless: true,
            args: ['--use-fake-ui-for-media-stream', '--use-fake-device-for-media-stream', '--no-sandbox', '--disable-notifications']
        });

        const context = this.browser.defaultBrowserContext();
        context.clearPermissionOverrides();
        context.overridePermissions(PageAdapter.URL, ['camera'])

        this.page = await this.browser.newPage();
        this.page.on('console', message => console.log(message.type(), message.text()));

        await this.page.evaluate( (authString : string) => {
            const auth = JSON.parse (authString)

            window ['localIces'] = [];
            window ['remoteIces'] = [];

            window['negotiated'] = false;

            window['peerConnection'] = new RTCPeerConnection ({
                iceTransportPolicy : 'relay',
                iceServers : [{
                    //urls : 'turn:35.217.48.206:3478?transport=udp',
                    urls : 'turn:[HIDDEN]?transport=udp',
                    credential : auth.password,
                    username : auth.username
                }]
            });
            
            (window['peerConnection'] as RTCPeerConnection).addEventListener ('icecandidate', event => {
                if (event.candidate != null) {

                    (window ['localIces'] as RTCIceCandidateInit []).push({                        
                        candidate : event.candidate.candidate,
                        sdpMLineIndex : event.candidate.sdpMLineIndex,
                        sdpMid : event.candidate.sdpMid
                    })
                }
            });             
        }, JSON.stringify (createTurnPassword ('login','password')))        
    }

    public readonly pushRemoteIces = (ice : RTCIceCandidateInit) => this.page.evaluate (iceString => {
        const negotiated = window['negotiated'];
        const remoteIces = (window ['remoteIces'] as RTCIceCandidateInit []);
        const peerConnection = (window['peerConnection'] as RTCPeerConnection);

        const ice = JSON.parse (iceString);

        if (negotiated == true) {
            return peerConnection.addIceCandidate (ice)
        }
        else {
            remoteIces.push (ice)
        }
    }, JSON.stringify (ice))

    public readonly getLocalIces = () => this.page.evaluate (() => {
        const localIces = (window ['localIces'] as RTCIceCandidateInit []);

        return JSON.stringify (localIces.splice (0, localIces.length))
    }) as Promise<string>;

    public readonly negotiateSdp = (offer : string) => this.page.evaluate (offer => {
        const peerConnection = (window['peerConnection'] as RTCPeerConnection);
        const remoteIces = (window ['remoteIces'] as RTCIceCandidateInit []);

        return peerConnection.setRemoteDescription ({
            sdp : offer,
            type : 'offer'
        }).then (() => peerConnection.createAnswer ({
            offerToReceiveVideo : true,
            offerToReceiveAudio : false
        }).then (answer => peerConnection.setLocalDescription (answer)
            .then (() => {

                window['negotiated'] = true;

                return Promise.all (remoteIces.splice (0, remoteIces.length).map (ice => peerConnection.addIceCandidate (ice)))
                    .then (() => answer.sdp)
            })
        ))

    }, offer) as Promise<string>

    public readonly getIceConnectionState = () => this.page.evaluate (()=> {
        const peerConnection = (window['peerConnection'] as RTCPeerConnection);

        if (peerConnection == null) {
            return null;
        }
        else {
            return peerConnection.iceConnectionState;
        }

    }) as Promise<RTCIceConnectionState>    

    public readonly getReceivedBytes = () => this.page.evaluate (()=> {
        const peerConnection = (window['peerConnection'] as RTCPeerConnection);

        if (peerConnection == null) {
            return 0;
        }
        else {
            return peerConnection.getStats ()
                .then (stat => {
                    
                    for (const key of stat.keys ()) {
                        const statItem = stat.get (key);

                        if (statItem.type == 'inbound-rtp') {
                            return statItem.bytesReceived;
                        }
                        
                    }
                    return 0;
                })  
        }

    }) as Promise<number>
}