import * as rx from "rxjs/Rx"

import {suite, skipOnError, only, timeout} from "mocha-typescript";
import {test} from "mocha-typescript";
import { JanusTestBase } from "../JanusTestBase";

import {FFmpegAdapter} from "./FFmpegAdapter";
import { PageAdapter } from "./PageAdapter";

@suite export class StreamingTest extends JanusTestBase {

    private readonly page = new PageAdapter ();
    
    @test @timeout (10000) async test () {
        const page = new PageAdapter ()

        await page.start ();
        try {
            const plugin = await (await this.connect ()).attachStreamingPlugin ().take (1).toPromise ();

            try {
                const ffmpeg = new FFmpegAdapter ();
                try {
                    const sdp = await ffmpeg.getSdpObservavble ().toPromise ();

                    console.log (sdp)

                    await plugin.createStream ({
                        id : 10000,
                        sdp,
                        videoport : 10000
                    }).toPromise ();

                    try {

                        const waitForBytesReceived = () => rx.Observable.interval (300)
                            .concatMap (() => rx.Observable.fromPromise (page.getReceivedBytes ())
                                .do (r => console.log ("Received bytes: " + r))
                                .filter (bytes => bytes > 10000)
                            )

                        const watchIceConnectionState = () => rx.Observable.interval (500)
                            .concatMap (() => rx.Observable.fromPromise (page.getIceConnectionState ())
                                .do (r => console.log ('Connection state:' + r))
                            )
                            .ignoreElements ()

                        const negotiatePageIces = () => rx.Observable.interval (300)
                            .concatMap (() => rx.Observable.fromPromise (page.getLocalIces ())
                                .map (iceListString => JSON.parse (iceListString) as RTCIceCandidateInit [])
                                .concatMap (list => rx.Observable.from (list)
                                    .do (ice => console.log ("Page ice: " + JSON.stringify (ice)))
                                    .concatMap (plugin.trickle)
                                )
                            ).ignoreElements ()

                        const negotiateJanusIces = () => plugin.getIceObservable ()
                            .do (ice => console.log ("Janus ice: " + JSON.stringify (ice)))
                            .concatMap (ice => rx.Observable.fromPromise (page.pushRemoteIces (ice)))
                            .ignoreElements ();

                        const negotiateSdp = () => plugin.getOffer (10000)
                            .do (console.log)
                            .concatMap (offer => rx.Observable.fromPromise (page.negotiateSdp (offer))
                                .do (console.log)
                                .concatMap (plugin.setAnswer)
                            ).ignoreElements ()

                        
                        await rx.Observable.merge (
                            waitForBytesReceived (),
                            watchIceConnectionState (),
                            negotiatePageIces (),
                            negotiateJanusIces (),
                            negotiateSdp (),
                        ).take (1).toPromise ();
                    }
                    finally {
                        await plugin.destroyStream (10000).toPromise ();
                    }

                }
                finally {
                    ffmpeg.stop ();
                }

            }
            finally {
                plugin.session.janus.close ();
            }
        }
        finally {
            await page.stop ();
        }
    }
}
