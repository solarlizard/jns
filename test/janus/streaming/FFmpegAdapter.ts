import * as proc from "child_process"
import * as rx from "rxjs/Rx";

export class FFmpegAdapter {
    private readonly sdpObservable = new rx.ReplaySubject<string> ()

    public readonly getSdpObservavble = () => this.sdpObservable.take (1);

    private readonly ffmmpeg = proc.spawn ('ffmpeg',`-re -stream_loop -1 -i ${__dirname}/vp8.webm -c:v copy -an -f rtp udp://127.0.0.1:10000?pkt_size=1300`.split (' '));

    constructor () {
        this.ffmmpeg.stdout.on ('data', data => {
            const value : string  = data.toString ();

            this.sdpObservable.next (value.substring (value.indexOf ('v=0')));
        })
    }

    public readonly stop = () => {
        this.ffmmpeg.kill ();
    }
}