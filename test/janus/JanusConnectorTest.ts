import * as rx from "rxjs/Rx"
import {suite, skipOnError, timeout, only} from "mocha-typescript";
import {assert} from "chai";
import {test} from "mocha-typescript";
import { JanusConnector, JanusStreamingPlugin, inject, Context } from "../../src";
import * as proc from "child_process";

@suite export class JanusConnectorTest {

    @inject private readonly instance : JanusConnector;
    @inject private readonly context : Context<any>

    @test async success () {
        await this.context.start (async () => {
            await this.instance.connectStreaming ('ws://127.0.0.1:8080').take (1).toPromise ();
        })
    }    

    @test @timeout (20000) async socketClose () {
        await this.context.start (async () => {

            // prepare
            const connectedObservable = new rx.ReplaySubject<JanusStreamingPlugin> (1)

            const subscription = this.instance.connectStreaming ('ws://127.0.0.1:8080')
                .do (plugin => connectedObservable.next (plugin))
                .subscribe ();

            const plugin = await connectedObservable.take (1).toPromise ();

            // test
            console.log (proc.execSync ('docker restart janus'));

            // assert
            await plugin.session.janus.socket.getCloseObservable ().toPromise ()
            subscription.unsubscribe ()
        })
    }    
}
