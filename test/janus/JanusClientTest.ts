import * as rx from "rxjs/Rx"
import {suite, skipOnError, timeout} from "mocha-typescript";
import {assert} from "chai";
import {test} from "mocha-typescript";

import * as http from "http"
import * as ws from "ws"

import { JanusClient, startNetworkServer, stopNetworkServer } from "../../src";

@suite export class JanusClientTest {
    protected readonly httpServer = new http.Server()
    protected readonly wsServer = new ws.Server({server: this.httpServer})

    public async before () {
        await startNetworkServer (this.httpServer, 8081);
    }

    public async after () {
        await stopNetworkServer (this.httpServer);
    }

    @test async requestError () {

        // prepare
        const inst = new JanusClient ('ws://127.0.0.1:8081');

        (inst.socket as any).sendRx = () => {
            console.log ("2324")
            return rx.Observable.throwError ("Error");
        }


        try {
            await inst.request ({}).toPromise ();
        }
        catch (error) {
            return;
        }

        assert.fail ()
    }


    @test async close () {

        // prepare
        const inst = new JanusClient ('ws://127.0.0.1:8081');

        inst.close ();

        await inst.socket.getCloseObservable ().toPromise ();
    }    
}
